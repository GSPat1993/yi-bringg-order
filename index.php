<?php
  /*
    Test Keys:

    Company ID: 3486
    Company Name: hawker.today
    Access Token: xC7sfGgtrPCcDnxRjcuh
    Secret Key: w-paKrUXx8VqiUV5mJLs

    Production Keys:

    Company ID: 3487
    Company Name: yihawker
    Access Token: HCapSArMmb9ji93gGMEf
    Secret Key: SxWtp1LJtLY7YUsxzM7B
  */
  header('Content-Type: application/json');
  header('Access-Control-Allow-Origin: *');

  $resultData = file_get_contents('php://input');
  $request = json_decode($resultData);

  // $company_id = "3486";
  // $access_token = "xC7sfGgtrPCcDnxRjcuh";
  // $secret_key = "w-paKrUXx8VqiUV5mJLs";

  $company_id = "3487";
  $access_token = "HCapSArMmb9ji93gGMEf";
  $secret_key = "SxWtp1LJtLY7YUsxzM7B";

  if($request->unit_no) {
    $unit_no = $request->unit_no;
  } else {
    $unit_no = "(No Unit Number)";    
  }

  if($request->customer_name) {
    $customer_name = $request->customer_name;
  } else {
    $customer_name = "No Name";    
  }

  if($request->phone) {
    $contact = $request->phone;
  } else {
    $contact = "No Phone";
  }

  if($request->email) {
    $email = $request->email;
  } else {
    $email = "No Email";
  }

  $create_customer_url = 'http://developer-api.bringg.com/partner_api/customers';

  $data_string_customer = array(
    'access_token' => $access_token,
    'timestamp' => date('Y-m-d H:i:s'),
    'name' => $customer_name,
    'company_id' => $company_id,
    'address' => "(ORDER-YI) ".$unit_no. ", " .$request->address,
    'phone' => $request->phone,
    'email' => $email
  );

  $signature_customer = hash_hmac("sha1", http_build_query($data_string_customer), $secret_key);
  $data_string_customer["signature"] = $signature_customer;
  $content_customer = json_encode($data_string_customer);

  $curl=curl_init();
  curl_setopt($curl, CURLOPT_URL, $create_customer_url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($curl, CURLOPT_POSTFIELDS, $content_customer);
  curl_setopt($curl, CURLOPT_HTTPHEADER,
  array('Content-Type:application/json',
  'Content-Length: ' . strlen($content_customer))
  );

  $json_response_customer = curl_exec($curl);

  $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

  if ( $status != 200 ) {
    die("Error: call to URL $create_customer_url failed with status $status, response $json_response_customer, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
  }

  curl_close($curl);

  $response_customer = json_decode($json_response_customer, true);

  /**************************************************************************************BUSINESS**************************************************************************************************/
  $create_business_url = 'http://developer-api.bringg.com/partner_api/customers';

  $data_string_business = array(
    'access_token' => $access_token,
    'timestamp' => date('Y-m-d H:i:s'),
    'name' => $request->business_name,
    'company_id' => $company_id,
    'address' => $request->business_address,
    'phone' => $request->business_phone,
    'email' => $request->business_email
  );

  $signature_business = hash_hmac("sha1", http_build_query($data_string_business), $secret_key);
  $data_string_business["signature"] = $signature_business;
  $content_business = json_encode($data_string_business);

  $curl=curl_init();
  curl_setopt($curl, CURLOPT_URL, $create_business_url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($curl, CURLOPT_POSTFIELDS, $content_business);
  curl_setopt($curl, CURLOPT_HTTPHEADER,
  array('Content-Type:application/json',
  'Content-Length: ' . strlen($content_business))
  );

  $json_response_business = curl_exec($curl);

  $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

  if ( $status != 200 ) {
    die("Error: call to URL $create_business_url failed with status $status, response $json_response_business, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
  }

  curl_close($curl);

  $response_business = json_decode($json_response_business, true);

  if($response_customer['success'] == true) {

    /***********************************************************************************TASK************************************************************************/
    $total = 0;
    $tmp = array();
    $order_text = "";

    foreach($request->dishes as $dish)
    {
        $tmp[$dish->stall_name]['dishes'][] = $dish;
    }

    foreach($tmp as $key => $stallPerDishes) {
      $order_text .= "--------------------------------------------------------------------------";
      $order_text .= "STALL_NAME: ".$key."; ";
      foreach($stallPerDishes as $dishes) {
        foreach($dishes as $dish) {
            $order_text .= "******************************************************************* ";
            $order_text .= "DISH: ".$dish->quantity." x ".$dish->name."; ";
            $order_text .= "PRICE: ".$dish->cost_price."; ";
            $order_text .= "******************************************************************* ";
            $total = $total + ($dish->cost_price * $dish->quantity);
        }
      }
    }
    // $order_text .= "STALL_NAME: ".$request->stall_name."; ";
    // foreach ($request->dishes as $dish) {
    //   $order_text .= "******************************************************************* ";
    //   $order_text .= "DISH: ".$dish->quantity." x ".$dish->name."; ";
    //   $order_text .= "PRICE: ".$dish->cost_price."; ";
    //   $order_text .= "******************************************************************* ";
    //   $total = $total + ($dish->cost_price * $dish->quantity);
    // }

      $order_text .= "--------------------------------------------------------------------------";
      $order_text .= "COMMENTS: ".$dish->comments."; ";
      $order_text .= "TOTAL AMOUNT: ".$total.";";

    $url = 'http://developer-api.bringg.com/partner_api/tasks';

    $data_string = array(
      'customer_id' => $response_business['customer']['id'],
      'company_id' => $company_id,
      'title' => "ORDER-YI-".$request->order_id,
      'note' => $order_text,
      'scheduled_at' => $request->schedule,
      // 'lat' => $response_customer['customer']['lat'],
      // 'lng' => $response_customer['customer']['lng'],
      'address' => $request->business_address,
      'total_price' => $request->total,
      // 'left_to_be_paid' => "left_to_be_paid",
      'shipping' => $request->delivery_fee,
      'User-Agent' => "Yi-today",
      'Client' => "Yi-today",
      'Client-Version' => "1.0",
      'access_token' =>  $access_token,
      'timestamp' => date('Y-m-d H:i:s')
    );

    $signature = hash_hmac("sha1", http_build_query($data_string), $secret_key);
    $data_string["signature"] = $signature;
    $content = json_encode($data_string);

    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
    array('Content-Type:application/json',
    'Content-Length: ' . strlen($content))
    );

    $json_response = curl_exec($ch);

    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ( $status != 200 ) {
      die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($ch) . ", curl_errno " . curl_errno($ch));
    }

    curl_close($ch);

    $response = json_decode($json_response, true);

    if($response['success'] == true) {

      /**************************************Create Waypoint******************************************************/

      $waypoint_url ='http://developer-api.bringg.com/partner_api/tasks/'.$response['task']['id'].'/way_points';

      $data_string_waypoint = array(
        'company_id' => $company_id,
        'customer_id' => $response_customer['customer']['id'],
        'scheduled_at' => $request->schedule,
        'address' => "(ORDER-YI)".$request->address,
        'lat' => $response_customer['customer']['lat'],
        'lng' => $response_customer['customer']['lng'],
        'access_token' => $access_token,
        'note' => "ORDER-YI",
        'timestamp' => date('Y-m-d H:i:s')
        );

      $signature_waypoint = hash_hmac("sha1", http_build_query($data_string_waypoint), $secret_key);
      $data_string_waypoint["signature"] = $signature_waypoint;
      $content_waypoint = json_encode($data_string_waypoint);

      $ch_waypoint=curl_init($waypoint_url);
      curl_setopt($ch_waypoint, CURLOPT_RETURNTRANSFER, true );
      curl_setopt($ch_waypoint, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch_waypoint, CURLOPT_POST, true);
      curl_setopt($ch_waypoint, CURLOPT_POSTFIELDS, $content_waypoint);
      curl_setopt($ch_waypoint, CURLOPT_HEADER, false);
      curl_setopt($ch_waypoint, CURLOPT_HTTPHEADER,array('Content-Type:application/json','Content-Length: ' . strlen($content_waypoint)));

      $json_responses_waypoint = curl_exec($ch_waypoint);

      $status_waypoint = curl_getinfo($ch_waypoint, CURLINFO_HTTP_CODE);

      if ( $status_waypoint != 200 ) {
        die("Error: call to URL $waypoint_url failed with status $status_waypoint, response $json_responses_waypoint, curl_error " . curl_error($ch_waypoint) . ", curl_errno " . curl_errno($ch_waypoint));
      }

      curl_close($ch_waypoint);
                
      $response_waypoint = json_decode($json_responses_waypoint, true);
      

      if($response_waypoint['success'] == true) {

        /*****************************************	for Notes *****************************************************************/		

        $noteHistory[] = array('Stall Name','Item','Quantity','Price');

        $tmp = array();

        foreach($request->dishes as $dish)
        {
            $tmp[$dish->stall_name]['dishes'][] = $dish;
        }

        foreach($tmp as $key => $stallPerDishes) {
          $noteHistory[] = array($key,'-','-','-');
          foreach($stallPerDishes as $dishes) {
            foreach($dishes as $dish) {
              $noteHistory[] = array('-',$dish->name,$dish->quantity,$dish->total_price);
            }
          }
        }
        // foreach ($request->dishes as $dish) {
        //   $noteHistory[] = array($dish->stall_name,$dish->name,$dish->quantity,$dish->total_price);
        // }
        $noteHistory[] = array('-','-','-','-');
        // $noteHistory[] = array('STALL NAME:','-',$request->stall_name);
        if($request->surcharge) {
          $noteHistory[] = array('SURCHARGE:','-',$request->surcharge);
        }
        $noteHistory[] = array('SERVICE FEE:','-',$request->service_fee);
        $noteHistory[] = array('DELIVERY FEE:','-',$request->delivery_fee);
        $noteHistory[] = array('COMMENT:','-',$request->comments);
        $noteHistory[] = array('TOTAL:','-',$request->total);

				
        
        $notes_url = 'http://developer-api.bringg.com/partner_api/tasks/'.$response['task']['id'].'/way_points/'.$response_waypoint['way_point']['id'].'/notes';

        $noteHistory = json_encode($noteHistory);		
        
        $data_string_notes = array(
        'company_id' => $company_id,
        'note' => $noteHistory,
        'type' =>  1,
        'access_token' => $access_token,
        'timestamp' => date('Y-m-d H:i:s')
        );
        
        $signature_notes = hash_hmac("sha1", http_build_query($data_string_notes), $secret_key);
        $data_string_notes["signature"] = $signature_notes;
        $content_notes = json_encode($data_string_notes);

        $ch_notes=curl_init($notes_url);
        curl_setopt($ch_notes, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch_notes, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch_notes, CURLOPT_POST, true);
        curl_setopt($ch_notes, CURLOPT_POSTFIELDS, $content_notes);
        curl_setopt($ch_notes, CURLOPT_HEADER, false);
        curl_setopt($ch_notes, CURLOPT_HTTPHEADER,array('Content-Type:application/json','Content-Length: ' . strlen($content_notes)));
        
        $json_response_notes = curl_exec($ch_notes);
        
        curl_close($ch_notes);
        
        $response_notes = json_decode($json_response_notes, true);
        print_r($response['task']['id']);
      }
    }
  }
?>